public class QueueWithoutNitemsApp {
    public static void main(String[] args){
        QueueWithoutNitems theQueue = new QueueWithoutNitems(7);

        theQueue.insert("BAM");
        theQueue.insert("PEERADA");
        theQueue.insert("WANGYAICHIM");
        theQueue.insert("TS");

        theQueue.remove();
        theQueue.remove();
        theQueue.remove();

        theQueue.insert("BB");
        theQueue.insert("PRD");
        theQueue.insert("13");
        theQueue.insert("SKY");
        theQueue.insert("MUSIC");
        int size = theQueue.size();
        System.out.println("Size Now = "+size);


        while(!theQueue.isEmpty()){
            String n = theQueue.remove();
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println();

        if(theQueue.isEmpty()==true){
            System.out.println("EMPTY");
        }else{
            System.err.println("NOT EMPTY");
        }

        size = theQueue.size();
        System.out.println("Size After Remove = "+size);
    }
}
